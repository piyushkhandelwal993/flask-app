from flask import Flask, render_template, request
from flask import send_from_directory
import csv
import os
import openai

app = Flask(__name__)
openai.api_key = os.environ["OPENAI_API_KEY"]

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        # Process the uploaded file
        if file.filename.endswith('.csv'):
            # Save the uploaded file to a specific location
            file_path = os.path.join('uploads', file.filename)
            file.save(file_path)

            # Generate a new file with the contents of the uploaded CSV file
            output_filename = file.filename[:-4] + '-completed.txt'
            output_file_path = os.path.join('uploads', output_filename)
            with open(file_path, 'r') as csv_file, open(output_file_path, 'w', newline='\n') as output_file:
                csvFile = csv.reader(csv_file)
                rows = list(csvFile)
                res = {}
                # displaying the contents of the CSV file
                for i,line in enumerate(rows):
                      print("\n##### Processing {0} of {1} ####".format(i+1,len(rows)))
                      name = line[0]
                      prompt = line[1] if line[1]!="" else file.filename
                      final_prompt = prompt + ":" + name
                      response = "response"
                      response = openai.Completion.create(
                          model="text-davinci-003",
                          prompt=final_prompt,
                          max_tokens=64,
                          temperature=1,
                          top_p=1.0,
                          frequency_penalty=0.0,
                          presence_penalty=0.0
                      )
                      res[final_prompt] = response['choices'][0]['text']

                for key, val in res.items():
                    output_file.write(key + " : " +  val + "\n")

            # Return the generated file for download
            return render_template('download.html', filename=output_filename)

    return render_template('upload.html')

@app.route('/download/<filename>')
def download(filename):
    uploads_dir = os.path.join(app.root_path, 'uploads')
    return send_from_directory(directory=uploads_dir, path=filename, as_attachment=True)

if __name__ == '__main__':
    app.run(debug=True)
